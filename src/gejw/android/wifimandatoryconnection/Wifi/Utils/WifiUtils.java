package gejw.android.wifimandatoryconnection.Wifi.Utils;

import android.net.wifi.WifiManager;

public class WifiUtils {

	public enum WifiState {
		_wifi_null, _wifi_disabled, _wifi_disabling, _wifi_enable, _wifi_enabling, _wifi_unknown
	}

	/**
	 * wifi状态转换为枚举型 ，方便使用
	 * 
	 * @param state
	 * @return
	 */
	public static WifiState State2State(int state) {
		WifiState wifiState = WifiState._wifi_null;
		switch (state) {
		case WifiManager.WIFI_STATE_DISABLED:
			wifiState = WifiState._wifi_disabled;
			break;
		case WifiManager.WIFI_STATE_DISABLING:
			wifiState = WifiState._wifi_disabling;
			break;
		case WifiManager.WIFI_STATE_ENABLED:
			wifiState = WifiState._wifi_enable;
			break;
		case WifiManager.WIFI_STATE_ENABLING:
			wifiState = WifiState._wifi_enabling;
			break;
		case WifiManager.WIFI_STATE_UNKNOWN:
			wifiState = WifiState._wifi_unknown;
			break;
		default:
			break;
		}

		return wifiState;
	}
}
