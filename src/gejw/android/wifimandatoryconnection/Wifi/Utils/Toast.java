package gejw.android.wifimandatoryconnection.Wifi.Utils;

import gejw.android.wifimandatoryconnection.R;
import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;

public class Toast {

	public static void show(Context context, String msg) {
		TextView txt = new TextView(context);
		txt.setText(msg);
		txt.setTextColor(0xFFFFFFFF);
		txt.setBackgroundResource(R.drawable.rectangle_666666);
		txt.setPadding(10, 5, 10, 5);
		txt.setGravity(Gravity.CENTER);
		android.widget.Toast toast = new android.widget.Toast(context);
		toast.setView(txt);
		toast.setDuration(android.widget.Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		System.out.println(msg);
	}
}
