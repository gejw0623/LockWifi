package gejw.android.wifimandatoryconnection;

import gejw.android.wifimandatoryconnection.Wifi.WIFITools;
import gejw.android.wifimandatoryconnection.Wifi.WIFITools.WifiCipherType;
import gejw.android.wifimandatoryconnection.Wifi.Listener.WifiListener.WifiChangeCallBack;
import gejw.android.wifimandatoryconnection.Wifi.Utils.WifiUtils.WifiState;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class MainActivity extends Activity {

	WIFITools mWifiTools = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mWifiTools = WIFITools.getInstance(this, new WifiChangeCallBack() {

			@Override
			public void WifiStateChanged(WifiState state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void WifiChanged(String ssid) {
				// TODO Auto-generated method stub

			}

			@Override
			public void WifiChanged() {
				// TODO Auto-generated method stub

			}
		});
		mWifiTools.startWifiListener();

		RadioGroup group = (RadioGroup) findViewById(R.id.radiogroup);
		group.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.btn1:
					cipherType = WifiCipherType.WIFICIPHER_NOPASS;
					break;
				case R.id.btn2:
					cipherType = WifiCipherType.WIFICIPHER_WEP;
					break;
				case R.id.btn3:
					cipherType = WifiCipherType.WIFICIPHER_WPA;
					break;

				default:
					break;
				}
			}
		});
	}

	WifiCipherType cipherType;

	public void Connect(View v) {
		String ssid = ((EditText) findViewById(R.id.et_ssid)).getText()
				.toString().trim();
		String password = ((EditText) findViewById(R.id.et_password)).getText()
				.toString().trim();
		mWifiTools.lockWifi(ssid, password, cipherType);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		WIFITools.getInstance(this, null).stopWifiListener();
	}

}
